

const  mongoose  = require('mongoose');

require('dotenv').config();

const MONGODB_URI = `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@cluster0.rbcj2.mongodb.net/${process.env.DATABASE}?retryWrites=true&w=majority`;

const dbConnection = async() => {
    try {
        await mongoose.connect(MONGODB_URI,{
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
        console.log('Error en la conexión');
    } catch (error) {
        throw new Error('Error a la hora de iniciar en la base de datos: '+ error);
    }
}

module.exports = {
    dbConnection
}


/*
const MongoClient = require("mongodb").MongoClient;

require('dotenv').config();

const MONGODB_URI = `mongodb+srv://${process.env.USER}:${process.env.PASSWORD}@cluster0.rbcj2.mongodb.net/${process.env.DATABASE}?retryWrites=true&w=majority`;
let cachedDb = null;

const dbConnection = async () => {
    if (cachedDb) {
        return cachedDb;
    }
    // Connect to our MongoDB database hosted on MongoDB Atlas
    const client = await MongoClient.connect(MONGODB_URI);
    // Specify which database we want to use
    const db = await client.db(`${process.env.DATABASE}`);
    cachedDb = db;
    return db;
}


module.exports = {
    dbConnection
}
*/
