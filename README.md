# APP HUMANS RESOURCES - ENDPOINTS
Este proyecto esta basado en desarrollar los principales endpoints para administrar un usuario utilizando AWS API GATEWAY y LAMBDA FUNCTION.
## Tecnologias utilizadas
- NodeJS.
- Serverless.
- Mongoose.
- moment-timezone.
- AWS (API Gateway,  Lambda Functions).
- CI/CD 
 	Integración continua, para agilizar el despliegue del proyecto y sea escalable en 
	 diferentes entornos (dev, qa, prod).
- .ENV con dotenv
- Git - GitLab
## Instalación
- Mongoose
Para abstraer todo de la base de datos, especialmente los documentos u objetos,
para este desafío el objeto Users. 

	`$ npm install mongoose`

- Moment-timezone
Para obtener la hora local de Lima

	`$ npm install moment-timezone`

- Dotenv
Para procesar las variables de entorno.

	`$ npm install dotenv`
## Documentación
`<link>` : https://gitlab.com/martinluque/endpoints-humans-resources/-/blob/develop/documentation/api_v1_openapi.json
## Endpoints
- Listar todos los usuarios con paginación.
> curl --location --request GET 'https://px30lhez83.execute-api.us-east-1.amazonaws.com/dev/users?limite=5&desde=10' \
--header 'x-api-key: zakFHgymw1w51MLsLML66nv8Ix694tQ5k8tPdmoj'

- Perfil de usuario por ID.
>curl --location --request GET 'https://px30lhez83.execute-api.us-east-1.amazonaws.com/dev/users/6144bfebf985e4459a64ca54' \
--header 'x-api-key: zakFHgymw1w51MLsLML66nv8Ix694tQ5k8tPdmoj'

- Crear un nuevo usuario.
> curl --location --request POST 'https://px30lhez83.execute-api.us-east-1.amazonaws.com/dev/users' \
--header 'x-api-key: zakFHgymw1w51MLsLML66nv8Ix694tQ5k8tPdmoj' \
--header 'Content-Type: application/json' \
--data-raw '{
    "email":"j.farfan@rpalatam.com.pe",
    "first_name":"Jefferson Agustin",
    "last_name":"farfan Villanueva",
    "date_of_birth":"05/O8/1990",
    "phone_number":"987393741",
    "office":"Backend Developer",
    "salary":"2500"
}'

- Actualizar perfil de usuario.
> curl --location --request PUT 'https://px30lhez83.execute-api.us-east-1.amazonaws.com/dev/users/6144bfebf985e4459a64ca54' \
--header 'x-api-key: zakFHgymw1w51MLsLML66nv8Ix694tQ5k8tPdmoj' \
--header 'Content-Type: application/json' \
--data-raw '{
    "first_name":"Christian Javier",
    "last_name":"Jimenez Rojas",
    "date_of_birth":"04/O7/1994",
    "phone_number":"987393742",
    "office":"Project Manajer",
    "salary":"7000"
}'

- Eliminar usuario por ID
> curl --location --request DELETE 'https://px30lhez83.execute-api.us-east-1.amazonaws.com/dev/users/6144bfebf985e4459a64ca54' \
--header 'x-api-key: zakFHgymw1w51MLsLML66nv8Ix694tQ5k8tPdmoj'
## Contribución
1. Crea un Fork del repositorio
2. Clonar en tu maquina mediante git clone https://gitlab.com/martinluque/endpoints-humans-resources.git
3. Crear una nueva rama
4. Realiza tus cambios
5. Manda tu pull request
## Mejoras en versiones posteriores
- Configurar los entornos para testing y producción.
- Configuracion el API Gateway y Lambda Function para mejorar los StatusCode.
- Mejorar la validación del Objeto User en el Schema.
## Licencia
MIT