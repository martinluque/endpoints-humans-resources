
const mongoose =  require('mongoose');
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    
    created_at: String,
    updated_at: String,
    email: String,
    phone_number: String,
    first_name: String,
    last_name: String,
    date_of_birth: String,
    attributes: {
        deparment: String,
        office: String,
        salary: String,
    }

})

const User = mongoose.model('User', usersSchema);

module.exports = {
    User
}
