
const moment = require('moment-timezone');

const { dbConnection } = require('./database/config');

const { User } = require('./models/User');


function responseMessage(results) {
  let statusCode = results.status;
  let data = results;
  let success = true;

  return {
    statusCode,
    headers: {
      "Access-Control-Allow-Headers": "*",
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "*",
      "Access-Control-Allow-Credentials": true,
      Accept: "*/*",
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ success, statusCode, data }),
  };
}

module.exports.list_users = async (event, context) => {
  let results = {};
  try {
    await dbConnection();
    
    const { limite = 5, desde = 0 } = event.queryStringParameters;
    const usuarios = await User.find()
      .skip(Number(desde))
      .limit(Number(limite));

    results.status = 200;
    results.users = usuarios;

  } catch (e) {
    results.message = 'Error del servidor: ' + e.message;
    results.status = 500;
  }

  return responseMessage(results);
};

module.exports.get_user = async (event, context) => {
  let results = {};
  try {
    
    let id = String(event.pathParameters.id) || "";
    await dbConnection();
    const usuario = await User.findById(id);

    let objUser = usuario || {};

    function isObjEmpty(obj) {
      return Object.keys(obj).length === 0;
    }

    if (isObjEmpty(objUser) == true) {
      results.message = 'Usuario no encontrado';
      results.status = 404;
    } else {
      results.message = 'Success';
      results.status = 200;
      results.user = objUser;
    }

  } catch (e) {
    results.message = 'Error del servidor: ' + e.message;
    results.status = 500;
  }
  return responseMessage(results);
};

module.exports.save_user = async (event, context) => {
  let results = {};
  try {
    let body = JSON.parse(event.body);

    let email = String(body.email) || "";
    let first_name = String(body.first_name) || "";
    let last_name = String(body.last_name) || "";
    let date_of_birth = String(body.date_of_birth) || "";
    let phone_number = String(body.phone_number) || "";
    let office = String(body.office) || "";
    let salary = String(body.salary) || "";

    if (email == '') {
      results.message = 'El servidor no pudo procesar su solicitud: email nulo o vacio';
      results.status = 404;
    } else {
      if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email)) {

        await dbConnection();
        const usuario = await User.findOne({ email: email });

        let objUser = usuario || {};

        function isObjEmpty(obj) {
          return Object.keys(obj).length === 0;
        }

        if (isObjEmpty(objUser) == true) {

          let today = moment().tz('America/Lima').format('MM/DD/YYYY HH:mm');

          let user = new User();
          user.created_at = today;
          user.updated_at = '';
          user.email = email;
          user.phone_number = phone_number;
          user.first_name = first_name;
          user.last_name = last_name;
          user.date_of_birth = date_of_birth;
          user.attributes.office = office;
          user.attributes.salary = salary;

          await user.save();

          results.message = 'Felicitaciones, usuario registrado';
          results.status = 200;
          results.user = user;

        } else {
          results.message = 'El email ingresado se encuentra asociado a otro colaborador';
          results.status = 404;
        }

      } else {
        results.message = 'El servidor no pudo procesar su solicitud: formato de email incorrecto';
        results.status = 404;
      }
    }

  } catch (e) {
    results.message = 'Error del servidor: ' + e.message;
    results.status = 500;
  }

  return responseMessage(results);
};

module.exports.put_user = async (event, context) => {
  let results = {};
  try {
    await dbConnection();
    let id = String(event.pathParameters.id) || "";

    let body = JSON.parse(event.body);

    let first_name = String(body.first_name) || "";
    let last_name = String(body.last_name) || "";
    let date_of_birth = String(body.date_of_birth) || "";
    let phone_number = String(body.phone_number) || "";
    let office = String(body.office) || "";
    let salary = String(body.salary) || "";

    const user = await User.findById(id);

    let objUser = user || {};

    function isObjEmpty(obj) {
      return Object.keys(obj).length === 0;
    }

    if (isObjEmpty(objUser) == true) {
      results.message = 'Usuario no encontrado';
      results.status = 404;
    } else {
      let today = moment().tz('America/Lima').format('MM/DD/YYYY HH:mm');

      let usuario = {
        updated_at: today,
        phone_number: phone_number,
        first_name: first_name,
        last_name: last_name,
        date_of_birth: date_of_birth,
        attributes: {
          office: office,
          salary: salary
        }
      }

      const result = await User.findByIdAndUpdate(id, { $set: usuario }, { new: true });
      results.message = 'Felicitaciones, datos actualizados del colaborador';
      results.status = 200;
      results.user = result;

    }
  } catch (e) {
    results.message = 'Error del servidor: ' + e.message;
    results.status = 500;
  }

  return responseMessage(results);
};

module.exports.delete_user = async (event, context) => {
  let results = {};
  try {
    let id = String(event.pathParameters.id) || "";

    await dbConnection();
    const result = await User.findByIdAndDelete(id);

    results.status = 200;
    results.message = 'Colaborador eliminado: ' + result;

  } catch (e) {
    results.status = 500;
    results.message = 'Error del servidor: ' + e.message;
  }

  return responseMessage(results);
};


